//Canvas variables
var frameInterval = 5; //in ms
var frameCount = 0;
var framePathSafety = 200;

var canvas = document.getElementById('canvas');
var canvasContext = canvas.getContext('2d');

//Draws the path
function drawPath() {
    if (canvas.getContext) {
        canvasContext.moveTo(pathPoints[0].x, pathPoints[0].y);
        canvasContext.beginPath();

        for (i = 1; i < pathPoints.length - 2; i++) {
            var xc = (pathPoints[i].x + pathPoints[i + 1].x) / 2;
            var yc = (pathPoints[i].y + pathPoints[i + 1].y) / 2;
            canvasContext.quadraticCurveTo(pathPoints[i].x, pathPoints[i].y, xc, yc);
        }
        canvasContext.quadraticCurveTo(pathPoints[i].x, pathPoints[i].y, pathPoints[i + 1].x, pathPoints[i + 1].y);

        canvasContext.lineTo(canvas.width, canvas.height);
        canvasContext.lineTo(0, canvas.height);

        canvasContext.strokeStyle = 'rgba(150, 100, 5, 255)';
        canvasContext.stroke();

        canvasContext.fillStyle = 'rgba(150, 100, 5, 255)';
        canvasContext.fill();

        canvasContext.closePath();
    }
}

//Gets path coords at position xpc by looking at every [0. canvas.height] y pixel for a path stroke
function getPathCoords(xpc) {
    var w = canvas.width;
    var d = canvasContext.getImageData(0, 0, w, canvas.height);
    var data = d.data;

    for (var yax = 0; yax <= canvas.height; yax++) {
        var red = data[((w * yax) + xpc) * 4];
        var green = data[((w * yax) + xpc) * 4 + 1];
        var blue = data[((w * yax) + xpc) * 4 + 2];
        var alpha = data[((w * yax) + xpc) * 4 + 3];

        if (red == 150 && green == 100 && blue == 5 && alpha == 255) {
            return { x: xpc, y: yax };
        }
    }
}

//Check if there is a pixel drawn at (xpc, ypc) of colour rgb(redc, greenc, bluec, alphac)
function isPixelDrawn(xpc, ypc, redc, greenc, bluec, alphac) {
    var w = canvas.width;
    var d = canvasContext.getImageData(0, 0, w, canvas.height);
    var data = d.data;

    xpc = Math.round(xpc);
    ypc = Math.round(ypc);

    var red = data[((w * ypc) + xpc) * 4];
    var green = data[((w * ypc) + xpc) * 4 + 1];
    var blue = data[((w * ypc) + xpc) * 4 + 2];
    var alpha = data[((w * ypc) + xpc) * 4 + 3];

    if (red == redc && green == greenc && blue == bluec && alpha == alphac) {
        return true;
    }
    else {
        return false;
    }
}

//Draws the sky
function drawSky() {
    canvasContext.fillStyle = "blue";
    canvasContext.fillRect(0, 0, canvas.width, canvas.height);
}

//Clears all canvas
function clearCanvas() {
    canvasContext.clearRect(0, 0, canvas.width, canvas.height);
}

//Initiates next frame after frameIntrvl ms.
function nextFrame(frameIntrvl) {
    frameCount++;
    setTimeout(function () { window.requestAnimationFrame(animation) }, frameIntrvl);
}