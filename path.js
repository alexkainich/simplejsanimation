//Path design variables
var pathStartX = 100;
var pathNextXDistance = 20;
var pathStartY = 100;
var pathYVariance = 110;
var pathScrolling = 2; //0 for off

var pathPoints = [{ x: 0, y: 80 }, { x: 10, y: 90 }];
var pathAddX = 0;

//Handles how many points we create for our path design
function definePathPointsOnScreen() {
    shiftPathPoints();

    if (frameCount % ((pathNextXDistance + pathScrolling) / pathScrolling) == 0) {
        addRandomPathPoint(pathStartX, pathStartY);
    }
    else if (frameCount < framePathSafety) {
        addRandomPathPoint(pathStartX, pathStartY);
    }
}

//Remove all points which disappear from the canvas when the canvas is scrolling
function shiftPathPoints() {
    for (var i = 0; i < pathPoints.length; i++) {
        if (pathPoints[i].x < 0) {
            pathPoints.shift();
        }
    }
}

//Gets a random continuation of the path positions starting from {startx, starty}
function addRandomPathPoint(startx, starty) {
    var random = Math.random();

    pathPoints.push({ x: startx + pathAddX * pathNextXDistance - pathScrolling * (frameCount - pathAddX), y: starty + Math.sin(random) * pathYVariance });
    pathAddX++;
}

//Applies scrolling to the path (the path moves from right to left and slowly disappears)
function turnOnPathScrolling(speed) {
    for (var j = 0; j < pathPoints.length; j++) {
        pathPoints[j].x = pathPoints[j].x - speed;
    }
}