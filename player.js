var keys = [];
var keySensitivity = 2;
var ballMaxSpeed = 3;

window.addEventListener("keydown", keysPressed, false);
window.addEventListener("keyup", keysReleased, false);

//When a key is pressed
function keysPressed(e) {
    keys[e.keyCode] = true;

    //Left arrow
    if (keys[37]) {
        if (ballForwardSpeed > - 2 * pathScrolling) {
            ballForwardSpeed = - 2 * pathScrolling - ballMaxSpeed;
        }

        e.preventDefault();
    }

    //Right arrow
    if (keys[39]) {
        if (ballForwardSpeed < 0) {
            ballForwardSpeed = ballMaxSpeed;
        }

        e.preventDefault();
    }
}

//When a key is released
function keysReleased(e) {
    keys[e.keyCode] = false;
    ballForwardSpeed = - pathScrolling;
}

//Check if player is still holding an arrow
function playerIsMoving() {
    if (keys[37]) {
        return "left";
    }

    if (keys[39]) {
        return "right";
    }
}

//Fix in case player holds both left and right arrows at the same time
function isPlayerMoving() {
    switch (playerIsMoving()) {
        case "right":
            if (ballForwardSpeed < 0) {
                ballForwardSpeed = ballMaxSpeed;
            }
            break;
        case "left":
            if (ballForwardSpeed > - 2 * pathScrolling) {
                ballForwardSpeed = - 2 * pathScrolling - ballMaxSpeed;
            }
            break;
    }
}