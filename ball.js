//Ball design variables
var ballCalculationStep = 5;
var ballRadius = 10;
var ballForwardSpeed = - pathScrolling;

var ballX = 300, ballY = 150;
var ballPushX = ballX; //global push of the ball per frame

//Draw the ball on the canvas
function drawBall(x, y, r) {
    canvasContext.beginPath();
    canvasContext.arc(x, y, r, 0, 2 * Math.PI);
    canvasContext.stroke();

    canvasContext.fillStyle = "red";
    canvasContext.fill();

    canvasContext.closePath();
}

//Move the ball "speed" pixels on each frame
function applyBallForwardMovement(speed) {
    ballPushX = ballPushX + speed;
}

//Find the point of distance ballRadius, on the vertical line (90 degrees) at point pathCoords[1], on line pathCoords[0] --> pathCoords[1]
function setBallCoords() {
    var pathCoords = [getPathCoords(ballPushX), getPathCoords(ballPushX + ballCalculationStep)];

    var radiants = Math.atan((pathCoords[1].y - pathCoords[0].y) / (pathCoords[1].x - pathCoords[0].x));
    ballX = Math.round(pathCoords[1].x + ballRadius * Math.sin(radiants));
    ballY = Math.round(pathCoords[1].y - ballRadius * Math.cos(radiants));
}