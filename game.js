   //Game variables
   var gameEnd = 0;

   //Main game function
   function animation() {
       clearCanvas();

       drawSky();

       designPath();

       designBall();

       applyBallForwardMovement(ballForwardSpeed);

       isMissileHit();

       if (gameEnd == 0) {
       designMissiles(missileSpeed);

       nextFrame(frameInterval);
       }
   }

   //Designs the path
   function designPath() {
       definePathPointsOnScreen();

       turnOnPathScrolling(pathScrolling);

       drawPath();
   }

   //Designs the ball
   function designBall() {
       isPlayerMoving();

       setBallCoords();

       drawBall(ballX, ballY, ballRadius);
   }

   //Designs the missiles
   function designMissiles(missileSpeed) {
       addMissile();

       drawMissiles();
   }