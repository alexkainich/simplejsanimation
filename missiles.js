//Missile variables
var missileSpeed = 2;
var missileLength = 5;
var missileCountMax = 20;

var missilePoints = [{ x: 800, y: 0 }, { x: 300, y: canvas.height }];
var missiles = [missilePoints];
var missileCount = 1;

//Removes missile that hit the ground
function isMissileGroundHit(i, missilePoints) {
    if (isPixelDrawn(missilePoints[1].x, missilePoints[1].y, 150, 100, 5, 255)) {
        missiles.splice(i, 1);
        missileCount--;
    }
}

//Removes missile if they escape the canvas
function isMissileOutOfBounds(i, missilePoints) {
    if (missilePoints[1].x < 0 || missilePoints[1].y < 0) {
        missiles.splice(i, 1);
        missileCount--;
    }
}

//Ends the game if a missile hits the ball
function isMissileBallHit(i, missilePoints) {
    if (missilePoints[1].x > ballX - ballRadius && missilePoints[1].x < ballX + ballRadius &&
        missilePoints[1].y > ballY - ballRadius && missilePoints[1].y < ballY + ballRadius) {
        gameEnd = 1;
    }
}

//Adds a missile up to a maximum number
function addMissile() {
    if (missileCount < missileCountMax) {
        var newMissile = [{ x: Math.random() * canvas.width, y: 0 }, { x: Math.random() * canvas.width, y: canvas.height }];

        missiles.push(newMissile);
        missileCount++;
    }
}

//Checks all missiles to see if they have hit the ball
function isMissileHit() {
    for (var i = 0; i < missiles.length; i++) {
        var missilePoints = missiles[i];

        isMissileOutOfBounds(i, missilePoints);
        isMissileGroundHit(i, missilePoints);
        isMissileBallHit(i, missilePoints);
    }
}

//Draws all missiles
function drawMissiles() {
    for (var i = 0; i < missiles.length; i++) {
        var missilePoints = missiles[i];
        var currentPoint0 = missilePoints[0];
        var currentPoint1 = missilePoints[1];
        var alpha = (currentPoint0.y - currentPoint1.y) / (currentPoint0.x - currentPoint1.x);
        var beta = (currentPoint1.y * currentPoint0.x - currentPoint0.y * currentPoint1.x) / (currentPoint0.x - currentPoint1.x);;

        if (currentPoint0.x < currentPoint1.x) {
            missilePoints[0].x = missilePoints[0].x + missileSpeed;
        }
        else if (currentPoint0.x > currentPoint1.x) {
            missilePoints[0].x = missilePoints[0].x - missileSpeed - pathScrolling;
        }

        if (currentPoint0.x == currentPoint1.x) {
            missilePoints[0].y = missileSpeed + beta;
        }
        else {
            missilePoints[0].y = alpha * missilePoints[0].x + beta;
        }

        missilePoints[1].x = missilePoints[0].x - missileLength;
        missilePoints[1].y = alpha * missilePoints[1].x + beta;

        canvasContext.beginPath();
        canvasContext.moveTo(missilePoints[0].x, missilePoints[0].y);
        canvasContext.lineTo(missilePoints[1].x, missilePoints[1].y);

        canvasContext.lineWidth = 5;
        canvasContext.strokeStyle = "#FFFF00";
        canvasContext.stroke();

        canvasContext.closePath();
    }
}